# Arduino_STM32_ST7789


## Description

Driver for ST7789-based IPS 240x240 SPI display.

Tested on STM32F103C8 "Blue Pill" board with LED on PC13 using Arduino_Core_STM32.

The actuall comms to the display are based on https://github.com/mireq/st7789-stm32-driver.

The example is based on https://github.com/adafruit/Adafruit-ST7735-Library.


## Arduino IDE dependencies:

 * Adafruit_GFX_Library.
 * Adafruit_ST7735_and_ST7789_Library.
 * Arduino_Core_STM32 in the Board Manager (i.e. 'STM32 cores').


## Wiring diagram:

| Display Pin | Blue Pill Pin | Comment                   |
|:-----------:|:-------------:| ------                    |
| GND         | G             | Ground                    |
| VCC         | 3.3           | Power Supply 3.3 V        |
| SCL         | PA5           | SPI1 CLK                  |
| SDA         | PA7           | SPI1 MOSI                 |
| RST         | PA9           | Reset                     |
| DC          | PA8           | Data/Command              |
| BLK         |  -            | Not Connected (backlight) |


## Blue pill preparation.

Install the HID bootloader (tested with version 2.2.2) from https://github.com/Serasidis/STM32_HID_Bootloader


## IDE Configuration (Tools menu): 

  Board: "Generic STM32F1 series"  
  Board part number: BluePill F103GB (or C8 with 128k)"  
  U(S)ART support: "Enabled (generic 'Serial')"  
  USB support (if abailable): "CDC (generic 'Serial' supersede U(S)ART)"  
  ..  
  ..  
  Upload method: "HID Bootloader 2.2"  

## Demo

See the [project wiki](https://gitlab.com/vesok/arduino_stm32_st7789/-/wikis/home)


## Known issues:

 * Adafruit_GFX::invertDisplay() does not work.
 * Adafruit_GFX::fillTriangle() does not work - see function mediabuttons() in the example.


## TODO:

 * Fix the known issues.
 * Method Arduino_STM32_ST7789::drawPixel() is implemented as "fill area" of size 1 pixel.
   This is likely inefficient but appears to work OK.
 * It is likely possible to override more Adafruit_GFX virtual methods with more efficient
   ones. Whenever time permits I'll go over the datasheet of ST7789. Patches are welcome.
 * Not tested on anything other than the Blue Pill board. Send me a board and I'll try to
   make it work.


