#include "Arduino_STM32_ST7789.h"

#include <stddef.h>
#include <stdint.h>
#include <stdbool.h>
#include <stm32f1xx.h>
#include <stm32f1xx_hal_rcc.h>


// stuff from vendor/cmsis/stm32f10x.h :
#define  DMA_CCR1_EN     ((uint16_t)0x0001)   /*!< Channel enable*/
#define  DMA_CCR1_DIR    ((uint16_t)0x0010)   /*!< Data transfer direction */
#define  DMA_CCR1_MINC   ((uint16_t)0x0080)   /*!< Memory increment mode */


/*

The code below has been copied from https://github.com/mireq/st7789-stm32-driver
with some minor modifications. The original copyright notice follows:

*/

/*
Copyright (c) 2019 Miroslav Bendík

Permission is hereby granted, free of charge, to any person obtaining a copy
of this software and associated documentation files (the "Software"), to deal
in the Software without restriction, including without limitation the rights
to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
copies of the Software, and to permit persons to whom the Software is
furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in all
copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
SOFTWARE.
*/

#define LCD_FILL_BUFFER_SIZE 64


// from the old header file:

#define ST7789_RST_PORT              GPIOA
#define ST7789_RST_PIN               GPIO_ODR_ODR9
#define ST7789_DC_PORT               GPIOA
#define ST7789_DC_PIN                GPIO_ODR_ODR8
#define ST7789_SPI                   SPI1
#define ST7789_DMA                   DMA1_Channel3

#define ST7789_PRESCALER             16
#define ST7789_OSC_MHZ               8

// System Function Command Table 1
#define ST7789_CMD_NOP               0x00 // No operation
#define ST7789_CMD_SWRESET           0x01 // Software reset
#define ST7789_CMD_RDDID             0x04 // Read display ID
#define ST7789_CMD_RDDST             0x09 // Read display status
#define ST7789_CMD_RDDPM             0x0a // Read display power
#define ST7789_CMD_RDDMADCTL         0x0b // Read display
#define ST7789_CMD_RDDCOLMOD         0x0c // Read display pixel
#define ST7789_CMD_RDDIM             0x0d // Read display image
#define ST7789_CMD_RDDSM             0x0e // Read display signal
#define ST7789_CMD_RDDSDR            0x0f // Read display self-diagnostic result
#define ST7789_CMD_SLPIN             0x10 // Sleep in
#define ST7789_CMD_SLPOUT            0x11 // Sleep out
#define ST7789_CMD_PTLON             0x12 // Partial mode on
#define ST7789_CMD_NORON             0x13 // Partial off (Normal)
#define ST7789_CMD_INVOFF            0x20 // Display inversion off
#define ST7789_CMD_INVON             0x21 // Display inversion on
#define ST7789_CMD_GAMSET            0x26 // Gamma set
#define ST7789_CMD_DISPOFF           0x28 // Display off
#define ST7789_CMD_DISPON            0x29 // Display on
#define ST7789_CMD_CASET             0x2a // Column address set
#define ST7789_CMD_RASET             0x2b // Row address set
#define ST7789_CMD_RAMWR             0x2c // Memory write
#define ST7789_CMD_RAMRD             0x2e // Memory read
#define ST7789_CMD_PTLAR             0x30 // Partial start/end address set
#define ST7789_CMD_VSCRDEF           0x33 // Vertical scrolling definition
#define ST7789_CMD_TEOFF             0x34 // Tearing line effect off
#define ST7789_CMD_TEON              0x35 // Tearing line effect on
#define ST7789_CMD_MADCTL            0x36 // Memory data access control
#define ST7789_CMD_VSCRSADD          0x37 // Vertical address scrolling
#define ST7789_CMD_IDMOFF            0x38 // Idle mode off
#define ST7789_CMD_IDMON             0x39 // Idle mode on
#define ST7789_CMD_COLMOD            0x3a // Interface pixel format
#define ST7789_CMD_RAMWRC            0x3c // Memory write continue
#define ST7789_CMD_RAMRDC            0x3e // Memory read continue
#define ST7789_CMD_TESCAN            0x44 // Set tear scanline
#define ST7789_CMD_RDTESCAN          0x45 // Get scanline
#define ST7789_CMD_WRDISBV           0x51 // Write display brightness
#define ST7789_CMD_RDDISBV           0x52 // Read display brightness value
#define ST7789_CMD_WRCTRLD           0x53 // Write CTRL display
#define ST7789_CMD_RDCTRLD           0x54 // Read CTRL value display
#define ST7789_CMD_WRCACE            0x55 // Write content adaptive brightness control and Color enhancemnet
#define ST7789_CMD_RDCABC            0x56 // Read content adaptive brightness control
#define ST7789_CMD_WRCABCMB          0x5e // Write CABC minimum brightness
#define ST7789_CMD_RDCABCMB          0x5f // Read CABC minimum brightness
#define ST7789_CMD_RDABCSDR          0x68 // Read Automatic Brightness Control Self-Diagnostic Result
#define ST7789_CMD_RDID1             0xda // Read ID1
#define ST7789_CMD_RDID2             0xdb // Read ID2
#define ST7789_CMD_RDID3             0xdc // Read ID3

// System Function Command Table 2
#define ST7789_CMD_RAMCTRL           0xb0 // RAM Control
#define ST7789_CMD_RGBCTRL           0xb1 // RGB Control
#define ST7789_CMD_PORCTRL           0xb2 // Porch control
#define ST7789_CMD_FRCTRL1           0xb3 // Frame Rate Control 1
#define ST7789_CMD_GCTRL             0xb7 // Gate control
#define ST7789_CMD_DGMEN             0xba // Digital Gamma Enable
#define ST7789_CMD_VCOMS             0xbb // VCOM Setting
#define ST7789_CMD_LCMCTRL           0xc0 // LCM Control
#define ST7789_CMD_IDSET             0xc1 // ID Setting
#define ST7789_CMD_VDVVRHEN          0xc2 // VDV and VRH Command enable
#define ST7789_CMD_VRHS              0xc3 // VRH Set
#define ST7789_CMD_VDVSET            0xc4 // VDV Setting
#define ST7789_CMD_VCMOFSET          0xc5 // VCOM Offset Set
#define ST7789_CMD_FRCTR2            0xc6 // FR Control 2
#define ST7789_CMD_CABCCTRL          0xc7 // CABC Control
#define ST7789_CMD_REGSEL1           0xc8 // Register value selection 1
#define ST7789_CMD_REGSEL2           0xca // Register value selection 2
#define ST7789_CMD_PWMFRSEL          0xcc // PWM Frequency Selection
#define ST7789_CMD_PWCTRL1           0xd0 // Power Control 1
#define ST7789_CMD_VAPVANEN          0xd2 // Enable VAP/VAN signal output
#define ST7789_CMD_CMD2EN            0xdf // Command 2 Enable
#define ST7789_CMD_PVGAMCTRL         0xe0 // Positive Voltage Gamma Control
#define ST7789_CMD_NVGAMCTRL         0xe1 // Negative voltage Gamma Control
#define ST7789_CMD_DGMLUTR           0xe2 // Digital Gamma Look-up Table for Red
#define ST7789_CMD_DGMLUTB           0xe3 // Digital Gamma Look-up Table for Blue
#define ST7789_CMD_GATECTRL          0xe4 // Gate control
#define ST7789_CMD_PWCTRL2           0xe8 // Power Control 2
#define ST7789_CMD_EQCTRL            0xe9 // Equalize Time Control
#define ST7789_CMD_PROMCTRL          0xec // Program Control
#define ST7789_CMD_PROMEN            0xfa // Program Mode Enable
#define ST7789_CMD_NVMSET            0xfc // NVM Setting
#define ST7789_CMD_PROMACT           0xfe // Program Action

#define ST7789_CMDLIST_END           0xff // End command (used for command list)

typedef struct st7789_Command {
  uint8_t command;
  uint8_t waitMs;
  uint8_t dataSize;
  const uint8_t *data;
} st7789_Command;

void st7789_WaitNanosecs(uint32_t nanosecs);
void st7789_Reset(void);
void st7789_StartCommand(void);
void st7789_StartData(void);
void st7789_WriteSpi(uint8_t data);
void st7789_ReadSpi(uint8_t *data, size_t length);
void st7789_WriteDMA(void *data, uint16_t length);
void st7789_WaitForDMA(void);
void st7789_ReadCommand(uint8_t command, void *data, size_t length);
void st7789_WriteCommand(uint8_t command, const void *data, size_t length);
void st7789_RunCommand(const st7789_Command *command);
void st7789_RunCommands(const st7789_Command *sequence);
void st7789_Init_1_3_LCD(int16_t w, int16_t h);
void st7789_StartMemoryWrite(void);
void st7789_SetWindow(uint16_t xStart, uint16_t yStart, uint16_t xEnd, uint16_t yEnd);
void st7789_FillArea(uint16_t color, uint16_t startX, uint16_t startY, uint16_t width, uint16_t height);
uint16_t st7789_RGBToColor(uint8_t r, uint8_t g, uint8_t b);
void st7789_GPIOInit(void);


// Weak attribute to allow override
void __attribute__((weak)) st7789_WaitNanosecs(uint32_t ns) {
  int ctr = ((ST7789_PRESCALER * ST7789_OSC_MHZ) * ns / 6);
  while (ctr) {
    __asm__ volatile ("nop");
    --ctr;
  }
}


void st7789_Reset(void) {
  ST7789_RST_PORT->ODR &= ~ST7789_RST_PIN;
  st7789_WaitNanosecs(10000); // Reset pulse time
  ST7789_RST_PORT->ODR |= ST7789_RST_PIN;
  st7789_WaitNanosecs(120000); // Maximum time of blanking sequence
}


void st7789_StartCommand(void) {
  //st7789_WaitNanosecs(10); //  D/CX setup time
  ST7789_DC_PORT->ODR &= ~ST7789_DC_PIN;
}


void st7789_StartData(void) {
  //st7789_WaitNanosecs(10); //  D/CX setup time
  ST7789_DC_PORT->ODR |= ST7789_DC_PIN;
}


void st7789_WriteSpi(uint8_t data) {
  for (int32_t i = 0; i<10000; i++) {
    if (ST7789_SPI->SR & SPI_SR_TXE) break;
  }
  ST7789_SPI->DR = data;
  while (ST7789_SPI->SR & SPI_SR_BSY);
}


void st7789_ReadSpi(uint8_t *data, size_t length) {
  // Disable SPI output
  ST7789_SPI->CR1 &= ~(SPI_CR1_BIDIOE);
  uint8_t dummy = 0;

  /*
  clockPulse(); // ???
  GPIOA->CRL = (GPIOA->CRL & ~(GPIO_CRL_CNF5));
  for (size_t i = 0; i < 1; ++i) {
    st7789_WaitNanosecs(10);
    GPIOA->ODR |= GPIO_ODR_ODR5;
    st7789_WaitNanosecs(10);
  }
  GPIOA->CRL = (GPIOA->CRL & ~(GPIO_CRL_CNF5)) | (GPIO_CRL_CNF5_1);
  */

  while (length--) {
    while (!(ST7789_SPI->SR & SPI_SR_TXE));
    ST7789_SPI->DR = dummy;
    while (!(ST7789_SPI->SR & SPI_SR_RXNE));
    *data++ = ST7789_SPI->DR;
  }
  while (ST7789_SPI->SR & SPI_SR_BSY);

  // Enable SPI output
  ST7789_SPI->CR1 |= SPI_CR1_BIDIOE;
}


void __attribute__((weak)) st7789_WriteDMA(void *data, uint16_t length) {
  ST7789_DMA->CCR =  (DMA_CCR1_MINC | DMA_CCR1_DIR); // Memory increment, direction to peripherial
  ST7789_DMA->CMAR  = (uint32_t)data; // Source address
  ST7789_DMA->CPAR  = (uint32_t)&ST7789_SPI->DR; // Destination address
  ST7789_DMA->CNDTR = length;
  ST7789_SPI->CR1 &= ~(SPI_CR1_SPE);  // Disable SPI
  ST7789_SPI->CR2 |= SPI_CR2_TXDMAEN; // Enable DMA transfer
  ST7789_SPI->CR1 |= SPI_CR1_SPE;     // Enable SPI
  ST7789_DMA->CCR |= DMA_CCR1_EN;     // Start DMA transfer
}


void st7789_WaitForDMA(void) {
  while(ST7789_DMA->CNDTR);
}

void st7789_ReadCommand(uint8_t command, void *data, size_t length) {
  st7789_StartCommand();
  st7789_WriteSpi(command);
  st7789_StartData();
  st7789_ReadSpi(reinterpret_cast<uint8_t *>(data), length);
}


void st7789_WriteCommand(uint8_t command, const void *data, size_t length) {
  st7789_StartCommand();
  st7789_WriteSpi(command);
  st7789_StartData();
  for (size_t i = 0; i < length; ++i) {
    st7789_WriteSpi(((const uint8_t *)data)[i]);
  }
}


void st7789_RunCommand(const st7789_Command *command) {
  st7789_StartCommand();
  st7789_WriteSpi(command->command);
  if (command->dataSize > 0) {
    st7789_StartData();
    for (uint8_t i = 0; i < command->dataSize; ++i) {
      st7789_WriteSpi(command->data[i]);
    }
  }
  if (command->waitMs > 0) {
    st7789_WaitNanosecs(command->waitMs * 1000);
  }
}


void st7789_RunCommands(const st7789_Command *sequence) {
  while (sequence->command != ST7789_CMDLIST_END) {
    st7789_RunCommand(sequence);
    sequence++;
  }
}


void st7789_Init_1_3_LCD(int16_t w, int16_t h) {
  // Resolution
  const uint8_t caset[4] = {
    0x00,
    0x00,
    (w - 1) >> 8,
    (w - 1) & 0xff
  };
  const uint8_t raset[4] = {
    0x00,
    0x00,
    (h - 1) >> 8,
    (h - 1) & 0xff
  };
  const st7789_Command initSequence[] = {
    // Sleep
    {ST7789_CMD_SLPIN, 10, 0, NULL},                    // Sleep
    {ST7789_CMD_SWRESET, 200, 0, NULL},                 // Reset
    {ST7789_CMD_SLPOUT, 120, 0, NULL},                  // Sleep out
    {ST7789_CMD_MADCTL, 0, 1, (const uint8_t *)"\x00"}, // Page / column address order
    {ST7789_CMD_COLMOD, 0, 1, (const uint8_t *)"\x55"}, // 16 bit RGB
    {ST7789_CMD_INVON, 0, 0, NULL},                     // Inversion on
    {ST7789_CMD_CASET, 0, 4, (const uint8_t *)&caset},  // Set width
    {ST7789_CMD_RASET, 0, 4, (const uint8_t *)&raset},  // Set height
    // Porch setting
    {ST7789_CMD_PORCTRL, 0, 5, (const uint8_t *)"\x0c\x0c\x00\x33\x33"},
    // Set VGH to 13.26V and VGL to -10.43V
    {ST7789_CMD_GCTRL, 0, 1, (const uint8_t *)"\x35"},
    // Set VCOM to 1.675V
    {ST7789_CMD_VCOMS, 0, 1, (const uint8_t *)"\x1f"},
    // LCM control
    {ST7789_CMD_LCMCTRL, 0, 1, (const uint8_t *)"\x2c"},
    // VDV/VRH command enable
    {ST7789_CMD_VDVVRHEN, 0, 2, (const uint8_t *)"\x01\xc3"},
    // VDV set to default value
    {ST7789_CMD_VDVSET, 0, 1, (const uint8_t *)"\x20"},
     // Set frame rate to 60Hz
    {ST7789_CMD_FRCTR2, 0, 1, (const uint8_t *)"\x0f"},
    // Set VDS to 2.3V, AVCL to -4.8V and AVDD to 6.8V
    {ST7789_CMD_PWCTRL1, 0, 2, (const uint8_t *)"\xa4\xa1"},
    // Gamma corection
    //{ST7789_CMD_PVGAMCTRL, 0, 14, (const uint8_t *)"\xd0\x08\x11\x08\x0c\x15\x39\x33\x50\x36\x13\x14\x29\x2d"},
    //{ST7789_CMD_NVGAMCTRL, 0, 14, (const uint8_t *)"\xd0\x08\x10\x08\x06\x06\x39\x44\x51\x0b\x16\x14\x2f\x31"},
    // Little endian
    {ST7789_CMD_RAMCTRL, 0, 2, (const uint8_t *)"\x00\x08"},
    {ST7789_CMDLIST_END, 0, 0, NULL},                   // End of commands
  };
  st7789_RunCommands(initSequence);
  st7789_FillArea(0, 0, 0, w, h);
  const st7789_Command initSequence2[] = {
    {ST7789_CMD_DISPON, 100, 0, NULL},                  // Display on
    {ST7789_CMD_SLPOUT, 100, 0, NULL},                  // Sleep out
    {ST7789_CMD_TEON, 0, 0, NULL},                      // Tearing line effect on
    {ST7789_CMDLIST_END, 0, 0, NULL},                   // End of commands
  };
  st7789_RunCommands(initSequence2);
}


void st7789_StartMemoryWrite(void) {
  st7789_StartCommand();
  st7789_WriteSpi(ST7789_CMD_RAMWR);
  st7789_StartData();
}


void st7789_SetWindow(uint16_t xStart, uint16_t yStart, uint16_t xEnd, uint16_t yEnd) {
  uint8_t caset[4];
  uint8_t raset[4];
  caset[0] = (uint8_t)(xStart >> 8);
  caset[1] = (uint8_t)(xStart & 0xff);
  caset[2] = (uint8_t)(xEnd >> 8);
  caset[3] = (uint8_t)(xEnd & 0xff);
  raset[0] = (uint8_t)(yStart >> 8);
  raset[1] = (uint8_t)(yStart & 0xff);
  raset[2] = (uint8_t)(yEnd >> 8);
  raset[3] = (uint8_t)(yEnd & 0xff);
  st7789_Command sequence[] = {
    {ST7789_CMD_CASET, 0, 4, caset},
    {ST7789_CMD_RASET, 0, 4, raset},
    {ST7789_CMD_RAMWR, 0, 0, NULL},
    {ST7789_CMDLIST_END, 0, 0, NULL},
  };
  st7789_StartCommand();
  st7789_RunCommands(sequence);
  st7789_StartData();
}


void st7789_FillArea(uint16_t color, uint16_t startX, uint16_t startY, uint16_t width, uint16_t height) {
  uint16_t buf[LCD_FILL_BUFFER_SIZE];
  for (size_t i = 0; i < LCD_FILL_BUFFER_SIZE; i++) {
    buf[i] = color;
  }
  st7789_SetWindow(startX, startY, startX + width - 1, startY + height - 1);
  size_t bytestToWrite = width * height * 2;
  uint16_t transferSize = (uint16_t)LCD_FILL_BUFFER_SIZE * 2;
  while (bytestToWrite > 0) {
    if (bytestToWrite < transferSize) {
      transferSize = bytestToWrite;
    }
    bytestToWrite -= transferSize;
    st7789_WriteDMA(&buf, transferSize);
    st7789_WaitForDMA();
  }
}


void st7789_SetPixel(uint16_t color, uint16_t x, uint16_t y) {
  st7789_SetWindow(x, y, x, y);
  st7789_WriteDMA(&color, 2);
  st7789_WaitForDMA();
}


uint16_t st7789_RGBToColor(uint8_t r, uint8_t g, uint8_t b) {
  return (((uint16_t)r >> 3) << 11) | (((uint16_t)g >> 2) << 5) | ((uint16_t)b >> 3);
}


void st7789_GPIOInit(void) {
  // Enable GPIOA and SPI1 clock
  RCC->APB2ENR |= RCC_APB2ENR_IOPAEN | RCC_APB2ENR_SPI1EN;
  // Enable DMA1 channel
  RCC->AHBENR |= RCC_AHBENR_DMA1EN;

  // Enable SPI
  ST7789_SPI->SR = 0;
  // Reverse polarity?
  ST7789_SPI->CR1 = \
    SPI_CR1_SSM | \
    SPI_CR1_SSI | \
    SPI_CR1_MSTR | \
    SPI_CR1_CPOL | \
    SPI_CR1_BIDIMODE | \
    SPI_CR1_BIDIOE;
  ST7789_SPI->CR1 |= SPI_CR1_SPE;

  // DC and RST signals
  // Maximum output speed
  ST7789_DC_PORT->CRH |= GPIO_CRH_MODE8;
  ST7789_RST_PORT->CRH |= GPIO_CRH_MODE9;
  // Output push pull
  ST7789_DC_PORT->CRH &= ~(GPIO_CRH_CNF8);
  ST7789_RST_PORT->CRH &= ~(GPIO_CRH_CNF9);

  // SPI pins
  // Maximum output speed on PA5/PA7
  GPIOA->CRL |= GPIO_CRL_MODE5;
  GPIOA->CRL |= GPIO_CRL_MODE7;
  // Alternate mode on PA5/PA7
  GPIOA->CRL = (GPIOA->CRL & ~(GPIO_CRL_CNF5)) | (GPIO_CRL_CNF5_1);
  GPIOA->CRL = (GPIOA->CRL & ~(GPIO_CRL_CNF7)) | (GPIO_CRL_CNF7_1);
}


// ========== class Arduino_STM32_ST7789 ==========

Arduino_STM32_ST7789::Arduino_STM32_ST7789(int16_t w, int16_t h) :
  Adafruit_GFX(w, h)
{
  st7789_GPIOInit();
  st7789_Reset();
  st7789_Init_1_3_LCD(w, h);
}

void Arduino_STM32_ST7789::drawPixel(int16_t x, int16_t y, uint16_t color)
{
  st7789_SetPixel(color, x, y);
}

void Arduino_STM32_ST7789::drawFastHLine(int16_t x, int16_t y, int16_t w, uint16_t color)
{
  if (w < 0) {
    st7789_FillArea(color, x + w, y, -w, 1);
  } else {
    st7789_FillArea(color, x, y, w, 1);
  }
}

void Arduino_STM32_ST7789::drawFastVLine(int16_t x, int16_t y, int16_t h, uint16_t color)
{
  if (h < 0) {
    st7789_FillArea(color, x, y + h, 1, -h);
  } else {
    st7789_FillArea(color, x, y, 1, h);
  }
}

void Arduino_STM32_ST7789::fillRect(int16_t x, int16_t y, int16_t w, int16_t h, uint16_t color)
{
  st7789_FillArea(color, x, y, w, h);
}

void Arduino_STM32_ST7789::fillScreen(uint16_t color)
{
  st7789_FillArea(color, 0, 0, width(), height());
}

