#ifndef ARDUINO_STM32_ST7789_H_SEEN
#define ARDUINO_STM32_ST7789_H_SEEN

#include <Adafruit_GFX.h>
#include <Adafruit_ST77xx.h>  //color definitions

/* 
MIT License

Copyright (c) 2020 vesok

Permission is hereby granted, free of charge, to any person obtaining a copy
of this software and associated documentation files (the "Software"), to deal
in the Software without restriction, including without limitation the rights
to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
copies of the Software, and to permit persons to whom the Software is
furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in all
copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
SOFTWARE.

*/ 

/* 
Driver for ST7789-based IPS 240x240 SPI display

Tested on STM32F103C8 "Blue Pill" board with LED on PC13 using Arduino_Core_STM32

The actuall comms to the display are based on https://github.com/mireq/st7789-stm32-driver

The example is based on https://github.com/adafruit/Adafruit-ST7735-Library

Requires these dependencies installed in the Arduino IDE:
 * Adafruit_GFX_Library
 * Adafruit_ST7735_and_ST7789_Library
 * Arduino_Core_STM32 in the Board Manager (i.e. 'STM32 cores')


Wiring diagram:

Display   Blue Pill
  Pin       Pin  
  ---       ---
  GND       G    // Ground
  VCC       3.3  // Power Supply 3.3 V
  SCL       PA5  // SPI1 CLK
  SDA       PA7  // SPI1 MOSI
  RST       PA9  // Reset
  DC        PA8  // Data/Command
  BLK       Not Connected (backlight)

  
Blue pill: install the HID bootloader (2.2.2) https://github.com/Serasidis/STM32_HID_Bootloader

IDE Configuration (Tools menu): 
  Board: "Generic STM32F1 series"
  Board part number: BluePill F103GB (or C8 with 128k)"
  U(S)ART support: "Enabled (generic 'Serial')"
  USB support (if abailable): "CDC (generic 'Serial' supersede U(S)ART)"
  ..
  ..
  Upload method: "HID Bootloader 2.2"


Known issues:

 * Adafruit_GFX::invertDisplay() does not work
 * Adafruit_GFX::fillTriangle() does not work - see function mediabuttons() in the example.


TODO:

 * Fix the known issues
 * Method Arduino_STM32_ST7789::drawPixel() is implemented as "fill area" of size 1 pixel.
   This is likely inefficient but appears to work OK.
 * It is likely possible to override more Adafruit_GFX virtual methods with more efficient
   ones. Whenever time permits I'll go over the datasheet of ST7789. Patches are welcome.
 * Not tested on anything other than the Blue Pill board. Send me a board and I'll try to
   make it work.

*/


class Arduino_STM32_ST7789 : public Adafruit_GFX {

public:

  Arduino_STM32_ST7789(int16_t w, int16_t h); 

  void drawPixel(int16_t x, int16_t y, uint16_t color) override;
  void drawFastHLine(int16_t x, int16_t y, int16_t w, uint16_t color) override;
  void drawFastVLine(int16_t x, int16_t y, int16_t h, uint16_t color) override;
  void fillRect(int16_t x, int16_t y, int16_t w, int16_t h, uint16_t color) override;
  void fillScreen(uint16_t color) override;

};

#endif // ARDUINO_STM32_ST7789_H_SEEN

